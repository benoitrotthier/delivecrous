import 'dart:convert';

import 'package:delivecrous/models/itemModel.dart';
import 'package:delivecrous/models/cartModel.dart';
import 'package:delivecrous/screens/itemDetails.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class ListItems extends StatefulWidget {
  ListItems();

  @override
  ListItemsState createState() => ListItemsState();
}

class ListItemsState extends State<ListItems> {
  List<ItemModel> itemsList = [];

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      itemsList = await _readJson();
      _loadCartFromStorage();
    });
  }

  @override
  Widget build(BuildContext context) {
    final double itemHeight = 400;
    final double itemWidth = 200;

    if (itemsList == null) {
      return CircularProgressIndicator();
    }

    return Expanded(
        child: Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: GridView.count(
              // Create a grid with 2 columns. If you change the scrollDirection to
              // horizontal, this produces 2 rows.
              crossAxisCount: 2,
              childAspectRatio: (itemWidth / itemHeight),
              // Generate 100 widgets that display their index in the List.
              children: List.generate(itemsList.length, (index) {
                return Card(
                    elevation: 10,
                    child: InkWell(
                        onTap: () {
                          _goToDetails(itemsList[index]);
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Image.asset(
                                'graphics/items/${itemsList[index].img}',
                                width: 170,
                                height: 150),
                            cardDetails(itemsList[index]),
                            cardCheckbox(itemsList[index])
                          ],
                        )));
              }),
            )));
  }

  Widget cardDetails(data) {
    return Container(
        padding: const EdgeInsets.all(20),
        child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Expanded(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                Container(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: Text(data.title,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ))),
                Text(
                  data.description,
                  style: TextStyle(color: Colors.grey),
                  overflow: TextOverflow.ellipsis,
                  softWrap: true,
                  maxLines: 4,
                ),
              ])),
          Text(data.price.toString())
        ]));
  }

  Widget cardCheckbox(ItemModel item) {
    return Align(
        alignment: Alignment.bottomRight,
        child: Padding(
          padding: EdgeInsets.only(top: 5),
          child: Checkbox(
              tristate: false,
              value: Provider.of<CartModel>(context).isInCart(item),
              activeColor: Colors.orange,
              onChanged: (bool newValue) {
                if (newValue == true) {
                  Provider.of<CartModel>(context, listen: false).addItem(item);
                } else {
                  Provider.of<CartModel>(context, listen: false)
                      .removeItem(item);
                }
                setState(() {
                  item.inCart = newValue;
                });
              }),
        ));
  }

  Future<List<ItemModel>> _readJson() async {
    var response = await rootBundle.loadString('jsonItems/items.json');
    List<ItemModel> itemsList = (json.decode(response) as List)
        .map((e) => ItemModel.fromJson(e))
        .toList();

    return itemsList;
  }

  _loadCartFromStorage() async {
    await Provider.of<CartModel>(context, listen: false).loadFromStorage();
    itemsList.forEach((item) {
      item.inCart =
          Provider.of<CartModel>(context, listen: false).isInCart(item);
    });
  }

  _goToDetails(ItemModel item) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ItemDetails(item)),
    );
  }
}
