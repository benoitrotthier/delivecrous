import 'package:flutter/material.dart';
import 'package:badges/badges.dart';
import 'package:provider/provider.dart';

import 'package:delivecrous/models/cartModel.dart';
import 'package:delivecrous/screens/cart.dart';

class CartButton extends StatefulWidget {
  CartButton(this.isCartPage, {Key key}) : super(key: key);
  final bool isCartPage;

  @override
  CartButtonState createState() => CartButtonState();
}

class CartButtonState extends State<CartButton> {
  @override
  Widget build(BuildContext context) {
    return Badge(
        position: BadgePosition.topEnd(top: 2, end: 4),
        badgeContent: Text(
            Provider.of<CartModel>(context).items.length.toString(),
            style: TextStyle(color: Colors.white)),
        child: IconButton(
          icon: Icon(Icons.shopping_cart, color: Colors.black),
          onPressed: () {
            if (!widget.isCartPage) {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Cart()),
              );
            }
          },
        ));
  }
}
