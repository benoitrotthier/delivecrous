import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:delivecrous/components/cartButton.dart';

class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  CustomAppBar(this.isCartPage, this.isSuccessPage, {Key key})
      : super(key: key);
  final bool isCartPage;
  final bool isSuccessPage;

  final String title = "Delivecrous";

  @override
  Size get preferredSize => const Size.fromHeight(100);

  @override
  CustomAppBarState createState() => CustomAppBarState();
}

class CustomAppBarState extends State<CustomAppBar> {
  double balance = 0.0;

  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _getBalanceFromStorage();
    });
  }

  void _getBalanceFromStorage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    double result = prefs.getDouble('balance') ?? 100.0;

    setState(() {
      balance = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        AppBar(
          backgroundColor: Color(0xFFFDF7EF),
          title: Text(
            "${widget.title}",
            style: TextStyle(
              color: Colors.black,
              fontFamily: "Roboto",
            ),
          ),
          centerTitle: true,
          automaticallyImplyLeading: !widget.isSuccessPage,
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
          actions: [
            Center(
                child: Text(
              "$balance €",
              style: TextStyle(
                color: Colors.black,
                fontFamily: "Roboto",
              ),
            )),
            CartButton(widget.isCartPage)
          ],
        ),
      ],
    );
  }
}
