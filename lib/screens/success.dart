import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:delivecrous/components/customAppBar.dart';
import 'package:delivecrous/screens/home.dart';

class Success extends StatefulWidget {
  Success(this.balance, {Key key}) : super(key: key);
  final double balance;

  @override
  SuccessState createState() => SuccessState();
}

class SuccessState extends State<Success> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            appBar: CustomAppBar(false, true),
            body: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image.asset('graphics/success/success.png',
                      fit: BoxFit.cover, height: 200, width: 120),
                  SizedBox(height: 5),
                  Text(
                    'Commande envoyée !',
                    style: TextStyle(
                        color: Colors.deepOrangeAccent,
                        fontWeight: FontWeight.bold,
                        fontSize: 30),
                  ),
                  SizedBox(height: 20),
                  Center(
                      child: Padding(
                          padding: EdgeInsets.only(left: 30, right: 30),
                          child: Text(
                              'Elle vous attendra à la fin de votre cours !',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                              )))),
                  SizedBox(height: 10),
                  Text(
                    'Solde restant : ${widget.balance} €',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 15,
                    ),
                  ),
                  SizedBox(height: 10),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Home(),
                        ),
                      );
                    },
                    child: Text("RETOUR À LA CARTE"),
                  )
                ],
              ),
            )));
  }
}
