import 'package:delivecrous/components/customAppBar.dart';
import 'package:delivecrous/components/listItems.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  double balance = 0.0;

  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      checkBalance();
    });
  }

  void checkBalance() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    double result = prefs.getDouble('balance') ?? 100.0;

    setState(() {
      balance = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar(false, false),
        body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "La carte",
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w300,
                  fontSize: 30,
                ),
              ),
              const Divider(
                  height: 5,
                  thickness: 1,
                  indent: 2,
                  endIndent: 2,
                  color: Colors.black),
              ListItems(),
            ]));
  }
}
