import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:delivecrous/components/customAppBar.dart';
import 'package:delivecrous/models/itemModel.dart';
import 'package:delivecrous/models/cartModel.dart';
import 'package:delivecrous/screens/success.dart';
import 'package:provider/provider.dart';

class Cart extends StatefulWidget {
  Cart({Key key}) : super(key: key);

  @override
  CartState createState() => CartState();
}

class CartState extends State<Cart> {
  List<ItemModel> cart;
  double balance = 0.0;

  @override
  void initState() {
    super.initState();

    _getBalanceFromStorage().then((_balance) {
      Provider.of<CartModel>(context, listen: false)
          .loadFromStorage()
          .then((_cart) {
        _cart.forEach((_item) {
          _item.inCart =
              Provider.of<CartModel>(context, listen: false).isInCart(_item);
        });
        setState(() {
          cart = List.of(_cart);
          balance = _balance;
        });
      });
    });
  }

  Future<double> _getBalanceFromStorage() {
    return SharedPreferences.getInstance().then((prefs) {
      return prefs.getDouble('balance') ?? 100.0;
    });
  }

  void _setBalanceToStorage(double _balance) {
    SharedPreferences.getInstance().then((prefs) {
      prefs.setDouble("balance", _balance);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar(true, false),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Votre panier",
                textScaleFactor: 3.0,
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w300,
                ),
              ),
              SizedBox(height: 20),
              //listItems(),
              if (cart != null)
                ...cart.map((item) => itemWidget(item)).toList(),
              SizedBox(height: 10),
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.only(right: 32),
                child: Text(
                  'Total : ' +
                      Provider.of<CartModel>(context).totalPrice().toString() +
                      ' €',
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(height: 10),
              Divider(
                  height: 5,
                  thickness: 1,
                  indent: 2,
                  endIndent: 2,
                  color: Colors.grey),
              SizedBox(height: 10),
              if (cart != null)
                if (cart.length > 0)
                  Container(
                    padding: EdgeInsets.only(left: 32, right: 32),
                    child: deliveryFooter(),
                  )
            ],
          ),
        ));
  }

  Widget itemWidget(ItemModel item) {
    return Material(
        elevation: 5,
        child: Container(
          padding: EdgeInsets.only(left: 32, right: 32, top: 10, bottom: 10),
          child: Row(
            children: [
              Container(
                  padding: EdgeInsets.only(right: 15),
                  child: Image.asset('graphics/items/${item.img}',
                      fit: BoxFit.cover, height: 90, width: 100)),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${item.title}',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      '${item.description}',
                      style: TextStyle(color: Colors.grey),
                      overflow: TextOverflow.ellipsis,
                      softWrap: true,
                      maxLines: 3,
                    ),
                  ],
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text("${item.price} €"),
                  Checkbox(
                      value: item.inCart,
                      activeColor: Colors.orange,
                      onChanged: (bool newValue) {
                        if (newValue == true) {
                          Provider.of<CartModel>(context, listen: false)
                              .addItem(item);

                          setState(() {
                            item.inCart = newValue;
                          });
                        } else {
                          setState(() {
                            cart =
                                Provider.of<CartModel>(context, listen: false)
                                    .removeItem(item);
                            item.inCart = newValue;
                          });
                        }
                      }),
                ],
              ),
            ],
          ),
        ));
  }

  Widget deliveryFooter() {
    return Column(children: <Widget>[
      Text(
        'Où veux-tu te faire livrer ? \n En salle de TD ?',
        textAlign: TextAlign.left,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
      ),
      SizedBox(height: 20),
      TextField(
        decoration:
            InputDecoration(border: OutlineInputBorder(), hintText: 'Rue'),
      ),
      TextField(
        decoration:
            InputDecoration(border: OutlineInputBorder(), hintText: 'Ville'),
      ),
      TextField(
        decoration: InputDecoration(
            border: OutlineInputBorder(), hintText: 'Code Postal'),
      ),
      ElevatedButton(
        onPressed: () {
          double total =
              Provider.of<CartModel>(context, listen: false).totalPrice();
          if (balance < total) {
            AlertDialog dialog = AlertDialog(
              title: Text("Erreur"),
              content: Text("Solde insuffisant"),
              actions: [
                ElevatedButton(
                    child: Text("OK"),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
              ],
            );

            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return dialog;
                });
          } else {
            balance -= total;
            _setBalanceToStorage(balance);
            Provider.of<CartModel>(context, listen: false).clear();
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Success(balance)),
            );
          }
        },
        child: Text("COMMANDER"),
      )
    ]);
  }
}
