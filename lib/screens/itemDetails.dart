import 'package:flutter/material.dart';
import 'package:delivecrous/components/customAppBar.dart';
import 'package:delivecrous/models/itemModel.dart';
import 'package:delivecrous/models/cartModel.dart';
import 'package:provider/provider.dart';

class ItemDetails extends StatefulWidget {
  ItemDetails(this.item, {Key key}) : super(key: key);
  final ItemModel item;

  @override
  ItemDetailsState createState() => ItemDetailsState();
}

class ItemDetailsState extends State<ItemDetails> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar(false, false),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image(image: AssetImage('graphics/items/${widget.item.img}')),
              titleSection(widget.item),
              descriptionSection(widget.item),
              cardCheckbox(widget.item),
              Container(
                  padding: const EdgeInsets.only(left: 32, right: 32),
                  child: Row(children: [
                    Expanded(
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Allergènes",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                )),
                            SizedBox(height: 10),
                            ...widget.item.allergens
                                .map((allergen) => Text('- $allergen'))
                                .toList(),
                            SizedBox(height: 50)
                          ]),
                    )
                  ]))
            ],
          ),
        ));
  }

  Widget titleSection(ItemModel item) {
    return Container(
      padding: const EdgeInsets.only(left: 32, right: 32, top: 10, bottom: 10),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    '${widget.item.title}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Text("${widget.item.price} €")
        ],
      ),
    );
  }

  Widget descriptionSection(ItemModel item) {
    return Container(
      padding: const EdgeInsets.only(left: 32, right: 32),
      child: Row(
        children: [
          Expanded(
            /*1*/
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /*2*/
                Text(
                  '${widget.item.description}',
                  style: TextStyle(
                    color: Colors.grey[500],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget cardCheckbox(ItemModel item) {
    return Align(
        alignment: Alignment.bottomRight,
        child: Padding(
          padding: EdgeInsets.only(top: 5),
          child: Checkbox(
              value: Provider.of<CartModel>(context).isInCart(item),
              activeColor: Colors.orange,
              onChanged: (bool newValue) {
                if (newValue == true) {
                  Provider.of<CartModel>(context, listen: false).addItem(item);
                } else {
                  Provider.of<CartModel>(context, listen: false)
                      .removeItem(item);
                }
                setState(() {
                  item.inCart = newValue;
                });
              }),
        ));
  }

  Widget allergensSection(ItemModel item) {
    return Container(
      padding: const EdgeInsets.only(left: 32, right: 32),
      child: Row(
        children: [
          Expanded(
            /*1*/
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /*2*/
                Text(
                  '${widget.item.allergens.map((allergen) => Text(allergen)).toList()}',
                  style: TextStyle(
                    color: Colors.grey[500],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
