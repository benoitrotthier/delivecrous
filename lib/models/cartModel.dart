import 'dart:convert';
import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:delivecrous/models/itemModel.dart';

class CartModel extends ChangeNotifier {
  final List<ItemModel> _items = [];

  UnmodifiableListView<ItemModel> get items => UnmodifiableListView(_items);

  CartModel();

  void addItem(ItemModel item) {
    _items.add(item);
    _persistToStorage();
    notifyListeners();
  }

  List<ItemModel> removeItem(ItemModel item) {
    _items.removeWhere((_item) => _item.title == item.title);
    _persistToStorage();
    notifyListeners();

    return _items;
  }

  void clear() {
    _items.clear();
    _persistToStorage();
    notifyListeners();
  }

  bool isInCart(ItemModel item) {
    bool result = false;
    _items.forEach((_item) {
      if (_item.title == item.title) {
        result = true;
      }
    });
    return result;
  }

  double totalPrice() {
    double total = 0.0;

    _items.forEach((_item) {
      total += _item.price;
    });

    return total;
  }

  void _persistToStorage() {
    SharedPreferences.getInstance().then((prefs) {
      prefs.setString("items", encode(_items));
    });
  }

  Future<List<ItemModel>> loadFromStorage() {
    return SharedPreferences.getInstance().then((prefs) {
      var cartJson = prefs.getString("items");
      List<ItemModel> cart = cartJson != null ? decode(cartJson) : List.empty();
      cart.forEach((item) {
        if (!isInCart(item)) {
          _items.add(item);
        }
      });

      return cart;
    });
  }

  String encode(List<ItemModel> items) => json.encode(
        items
            .map<Map<String, dynamic>>((item) => ItemModel.toMap(item))
            .toList(),
      );

  List<ItemModel> decode(String items) => (json.decode(items) as List<dynamic>)
      .map<ItemModel>((item) => ItemModel.fromJson(item))
      .toList();
}
