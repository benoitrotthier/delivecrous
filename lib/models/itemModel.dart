import 'dart:convert';

class ItemModel {
  String img;
  String title;
  String description;
  double price;
  bool inCart;
  List<String> allergens;

  ItemModel(String img, String title, String description, double price, List<String> allergens) {
    this.img = img;
    this.title = title;
    this.description = description;
    this.price = price;
    this.inCart = false;
    this.allergens = allergens;
  }

  ItemModel.fromJson(Map<String, dynamic> json) {
    img = json["image"];
    title = json["title"];
    description = json["description"];
    price = json['price'] == null ? 0.0 : json['price'].toDouble();
    inCart = json["inCart"];
    var allergensJson = json["allergens"];
    if(allergensJson == null) {
      allergens = new List.empty();
    }
    else {
      allergens = new List<String>.from(allergensJson);
    }
  }

  @override
  String toString() {
    return "$title, $description, $img, $price, $allergens";
  }

  static Map<String, dynamic> toMap(ItemModel itemModel) => {
        'title': itemModel.title,
        'description': itemModel.description,
        'image': itemModel.img,
        'price': itemModel.price,
        'allergens': List.from(itemModel.allergens)
      };

  static String encode(List<ItemModel> items) => json.encode(
        items
            .map<Map<String, dynamic>>((item) => ItemModel.toMap(item))
            .toList(),
      );

  static List<ItemModel> decode(String items) =>
      (json.decode(items) as List<dynamic>)
          .map<ItemModel>((item) => ItemModel.fromJson(item))
          .toList();
}
